<?php
/*
Plugin Name: WP Content Nav
Description: WP Content Nav Description
Version: 1.5.0
Author: Author
Author URI: http://westex.de/
*/

define('WP_CONTENT_NAV_PLUGIN_ROOT', __FILE__);
define('WP_CONTENT_NAV_PLUGIN_ROOT_DIR', plugin_dir_path(WP_CONTENT_NAV_PLUGIN_ROOT));
define('WP_CONTENT_NAV_PLUGIN_ROOT_URL', plugins_url('', WP_CONTENT_NAV_PLUGIN_ROOT));

include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/admin/Install.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/Common.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/visitors/Common.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/admin/Common.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/Helper.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/visitors/Helper.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/admin/PostMetaBox.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/admin/Settings.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/admin/SettingsIntegration.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/visitors/Navigation.php';
include WP_CONTENT_NAV_PLUGIN_ROOT_DIR . 'core/controllers/visitors/Widget.php';

$WPContentNavInstall = new WPContentNavInstall();