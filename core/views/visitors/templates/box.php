<div data-wp-content-nav-template="box" id="wpContentNavContent" 
     class="wpContentNavContent <?php echo @$data['root']['class']?>"
    <?php echo $data['root']['attr'] ?>>
    <ul>
        <?php
        foreach ($data['this']->postTags as $postTag)
        {
            echo "<li><a href='#{$postTag['slug']}'>{$postTag['text']}</a></li>";
        }
        ?>
    </ul>
</div>