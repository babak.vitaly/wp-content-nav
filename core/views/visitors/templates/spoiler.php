<div id="wpContentNavContent" data-wp-content-nav-template="spoiler"
     class="wpContentNavContent <?php echo @$data['root']['class'] ?>"
    <?php echo $data['root']['attr'] ?>>
    <?php
    $h = new WPContentNavVisitorsHelper();
    $postTags = array();
    $parentTag = $data['this']->postSettings['templates']['spoiler']['parentTag'];

    foreach ($data['this']->postTags as $postTagIndex => $postTag)
    {
        if ($h->getTagNum($postTag['tag']) < $h->getTagNum($parentTag)) continue;
        $postTags[] = $postTag;
    }
    $h->generateList($postTags, array('childIconName' => $data['this']->style['sections']['parentAndChild']['childIconName']));
    ?>
</div>