<div id="wpContentNavAreaFree" class="wpContentNav <?php echo @$data['root']['class'] ?>"
    <?php echo @$data['root']['attr']?>>
    <div class="wpContentNavHeader">
        <i class="fa <?php echo $data['this']->style['sections']['blockTitle']['iconName'] ?>" aria-hidden="true"></i>
        <span class="wpContentNavTitle"><?php echo @$data['this']->style['sections']['blockTitle']['text'] ?></span>
    </div>
    <div class="wpContentNavBody">
        <?php echo @$data['body']['html'] ?>
    </div>
</div>