<?php
$h = new WPContentNavHelper();
global $WPContentNavVisitorsCommon;
$WPContentNavVisitorsCommon->loadViewFile("visitors/css/common", $data, true);

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="spoiler"] a {<?php

$p =& $data['sections']['itemColors']['default'];
if (@$p != '') echo "background:{$p};";

$p =& $data['sections']['fontDefault']['size'];
if (@$p != '') echo "font-size:{$p}px;";

$p =& $data['sections']['fontDefault']['weight'];
if (@$p != '') echo "font-weight:{$p};";

$p =& $data['sections']['fontDefault']['defaultColor'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="spoiler"] .wpContentNavParent>a:before {<?php

switch ($data['sections']['parentAndChild']['parentIconName'])
{
    case 'plusAndMinus':
        echo 'content: "\f067";';
        break;
    case 'arrows':
    default:
        echo 'content: "\f105";';
        break;
}

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="spoiler"] .wpContentNavParent.wpContentNavActive.wpContentNavHasChild>a:before {<?php

switch ($data['sections']['parentAndChild']['parentIconName'])
{
    case 'plusAndMinus':
        echo 'content: "\f068";';
        break;
    case 'arrows':
    default:
        echo 'content: "\f107";';
        break;
}

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="spoiler"] a.wpContentNavActive {<?php

$p =& $data['sections']['fontActive']['size'];
if (@$p != '') echo "font-size:{$p}px;";

$p =& $data['sections']['fontActive']['weight'];
if (@$p != '') echo "font-weight:{$p};";

$p =& $data['sections']['fontActive']['color'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="spoiler"] a.wpContentNavActive:after {<?php

$p =& $data['sections']['itemColors']['active'];
if (@$p != '') echo "background:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="spoiler"] a:hover {<?php

$p =& $data['sections']['fontDefault']['hoverColor'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="spoiler"] a:after {<?php

$p =& $data['sections']['itemColors']['mouseOver'];
if (@$p != '') echo "background:{$p};!important";

?>}
