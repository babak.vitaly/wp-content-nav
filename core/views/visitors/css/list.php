<?php
$h = new WPContentNavHelper();
global $WPContentNavVisitorsCommon;
$WPContentNavVisitorsCommon->loadViewFile("visitors/css/common", $data, true);

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"] a {<?php

$p =& $data['sections']['itemColors']['default'];
if (@$p != '') echo "background:{$p};";

$p =& $data['sections']['fontDefault']['size'];
if (@$p != '') echo "font-size:{$p}px;";

$p =& $data['sections']['fontDefault']['weight'];
if (@$p != '') echo "font-weight:{$p};";

$p =& $data['sections']['fontDefault']['defaultColor'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"] a:before {<?php

$p =& $data['sections']['listStyle']['prefix'];
if (@$p != '') echo "content:'{$p}';";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"] a.wpContentNavActive {<?php

$p =& $data['sections']['fontActive']['size'];
if (@$p != '') echo "font-size:{$p}px;";

$p =& $data['sections']['fontActive']['weight'];
if (@$p != '') echo "font-weight:{$p};";

$p =& $data['sections']['fontActive']['color'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"] a.wpContentNavActive:after {<?php

$p =& $data['sections']['itemColors']['active'];
if (@$p != '') echo "background:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"] a:hover {<?php

$p =& $data['sections']['fontDefault']['hoverColor'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"] a:after {<?php

$p =& $data['sections']['itemColors']['mouseOver'];
if (@$p != '') echo "background:{$p};!important";

?>}<?php

//Effects.
//Effect "leftToRight"

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="leftToRight"] a:after {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'width');
echo $h->generateCompatibleCSSProperty('transition-duration', '500ms');

?>}<?php

//Effect "rightToLeft"

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="rightToLeft"] a:after {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'width');
echo $h->generateCompatibleCSSProperty('transition-duration', '500ms');
echo "left:initial;";
echo "right:0;";

?>}<?php

//Effect "topToBottom"

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="topToBottom"] a:after {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'height');
echo $h->generateCompatibleCSSProperty('transition-duration', '500ms');
echo "bottom:initial;";
echo "height:0;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="topToBottom"] a.wpContentNavActive:after {<?php

echo "height:100%;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="topToBottom"] a:hover:after {<?php

echo "height:100%;";

?>}<?php

//Effect "bottomToTop"

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="bottomToTop"] a:after {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'height');
echo $h->generateCompatibleCSSProperty('transition-duration', '500ms');
echo "top:initial;";
echo "height:0;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="bottomToTop"] a.wpContentNavActive:after {<?php

echo "height:100%;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="bottomToTop"] a:hover:after {<?php

echo "height:100%;";

?>}<?php

//Effect "fadeIn"

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="fadeIn"] a:after {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'opacity');
echo $h->generateCompatibleCSSProperty('transition-duration', '1000ms');
echo "opacity:0;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="fadeIn"] a.wpContentNavActive:after {<?php

echo "opacity:1;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="fadeIn"] a:hover:after {<?php

echo "opacity:1;";

?>}<?php

//Effect "zoom"

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="zoom"] a:after {<?php

echo "width:0;";
echo "height:0;";
echo "top:50%;";
echo "left:50%;";
echo $h->generateCompatibleCSSProperty('transform', 'translate(-50%, -50%)');
echo $h->generateCompatibleCSSProperty('transition-property', 'width, height');
echo $h->generateCompatibleCSSProperty('transition-duration', '500ms');

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="zoom"] a.wpContentNavActive:after {<?php

echo "width:100%;";
echo "height:100%;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="list"][data-wp-content-nav-effect="zoom"] a:hover:after {<?php

echo "width:100%;";
echo "height:100%;";

?>}