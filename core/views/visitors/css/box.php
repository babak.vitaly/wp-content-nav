<?php
$h = new WPContentNavHelper();
global $WPContentNavVisitorsCommon;
$WPContentNavVisitorsCommon->loadViewFile("visitors/css/common", $data, true);

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"] a {<?php

$p =& $data['sections']['itemColors']['default'];
if (@$p != '') echo "background:{$p};";

$p =& $data['sections']['itemColors']['active'];
if (@$p != '') echo "border-color:transparent transparent transparent {$p};";

$p =& $data['sections']['fontDefault']['size'];
if (@$p != '') echo "font-size:{$p}px;";

$p =& $data['sections']['fontDefault']['weight'];
if (@$p != '') echo "font-weight:{$p};";

$p =& $data['sections']['fontDefault']['defaultColor'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"] a:hover {
<?php
$p =& $data['sections']['itemColors']['mouseOver'];
if (@$p != '') echo "background:{$p};";

$p =& $data['sections']['fontDefault']['hoverColor'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"] a.wpContentNavActive {<?php

$p =& $data['sections']['itemColors']['active'];
if (@$p != '') echo "background:{$p};";

$p =& $data['sections']['fontActive']['size'];
if (@$p != '') echo "font-size:{$p}px;";

$p =& $data['sections']['fontActive']['weight'];
if (@$p != '') echo "font-weight:{$p};";

$p =& $data['sections']['fontActive']['color'];
if (@$p != '') echo "color:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"] a.wpContentNavActive:hover {<?php

$p =& $data['sections']['itemColors']['active'];
if (@$p != '') echo "background:{$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"] a.wpContentNavActive:before {<?php

$p =& $data['sections']['itemColors']['active'];
if (@$p != '') echo "border-color:transparent {$p} transparent transparent;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"].wpContentNavAlignLeft a.wpContentNavActive:before {<?php

$p =& $data['sections']['itemColors']['active'];
if (@$p != '') echo "border-color:transparent transparent transparent {$p};";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"].wpContentNavAlignLeft a{<?php

$p =& $data['sections']['itemColors']['active'];
if (@$p != '') echo "border-color:transparent {$p} transparent transparent;";

?>}<?php

//Effects.
//Effect "leftToRight"

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"][data-wp-content-nav-effect="leftToRight"] a:after {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'width');
echo $h->generateCompatibleCSSProperty('transition-duration', '500ms');

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"][data-wp-content-nav-effect="leftToRight"].wpContentNavAlignLeft a.wpContentNavActive:before {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'right, visibility');
echo $h->generateCompatibleCSSProperty('transition-duration', '200ms');
echo $h->generateCompatibleCSSProperty('transition-delay', '450ms');

?>}<?php

//Effect "rightToLeft"

?>#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"][data-wp-content-nav-effect="rightToLeft"] a:after {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'width');
echo $h->generateCompatibleCSSProperty('transition-duration', '500ms');
echo "left:initial;";
echo "right:0;";

?>}#wpContentNavContent.wpContentNavContent[data-wp-content-nav-template="box"][data-wp-content-nav-effect="rightToLeft"]:not(.wpContentNavAlignLeft) a.wpContentNavActive:before {<?php

echo $h->generateCompatibleCSSProperty('transition-property', 'left, visibility');
echo $h->generateCompatibleCSSProperty('transition-duration', '200ms');
echo $h->generateCompatibleCSSProperty('transition-delay', '450ms');

?>}


