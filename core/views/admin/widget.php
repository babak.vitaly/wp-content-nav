<p>
    <label for="<?php echo $data['wpWidget']->get_field_id('title'); ?>">Title</label>
    <input id="<?php echo $data['wpWidget']->get_field_id('title'); ?>"
           name="<?php echo $data['wpWidget']->get_field_name('title'); ?>"
           value="<?php echo $data['instance']['title']; ?>" style="width:100%;">
</p>
<p>
    You may set a content from the Post Meta Box.<br>
    If the title field is empty, a title row not display.<br>
</p>