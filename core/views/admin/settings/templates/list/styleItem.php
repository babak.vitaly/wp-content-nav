<?php
$sections = &$data['data']['sections'];
$h = new WPContentNavHelper();
?>
<div class="wpContentNavRowItem wpContentNavClosed <?php echo @$data['root']['class'] ?>"
     data-wp-content-nav-id="<?php echo @$data['id'] ?>">
    <div class="wpContentNavHeader wpContentNavHeaderStyle_1">
        <span class="wpContentNavText wpContentNavEditableTitle" data-wp-cprty-max-length="100">
            <span class="wpContentNavEditableTitleText"><?php echo @$data['data']['name'] ?></span>
            <i class="fa fa-pencil wpContentNavBtn wpContentNavEdit wpContentNavEditableTitleEdit"
               aria-hidden="true"></i>
        </span>
        <span class="wpContentNavBtnDetail">
          <i class="fa fa-angle-up" aria-hidden="true"></i>
        </span>
        <div class="wpContentNavBtn wpContentNavBtnAdd wpContentNavBtnActionRemove">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </div>
    </div>
    <div class="wpContentNavContent wpContentNavContentStyle_1">
        <div class="wpContentNavConf" data-wp-content-nav-section="blockTitle">
            <div class="wpContentNavName">Block Title</div>
            <div class="wpContentNavValue">
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Text</span>
                        <input type="text" class="form-control" data-wp-content-nav-parameter="text"
                               value="<?php echo @$sections['blockTitle']['text'] ?>">
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <span class="wpContentNavParameterTitle">Icon</span>
                    <div class="wpContentNavIconBox">
                        <div class="wpContentNavControl">
                            <input type="text" placeholder="Example: fa-info-circle"
                                   class="form-control wpContentNavIconName" maxlength="100"
                                   data-wp-content-nav-parameter="iconName"
                                   data-wp-content-nav-type="iconBox"
                                   value="<?php echo @$sections['blockTitle']['iconName'] ?>">
                        </div>
                        <div class="wpContentNavIconPreview">
                            <i class="fa <?php echo @$sections['blockTitle']['iconName'] ?>" aria-hidden="true"></i>
                        </div>
                        <br>
                        <a href="http://fontawesome.io/icons/" target="_blank" title="Click here to open icon list.">
                            Icon List
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="wpContentNavConf" data-wp-content-nav-section="effects">
            <div class="wpContentNavName">Effects</div>
            <div class="wpContentNavValue">
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle"></span>
                        <?php echo $h->generateSelect(array(
                            'attr' => 'class="form-control" data-wp-content-nav-parameter="name"',
                            'value' => &$sections['effects']['name'],
                            'options' => array(
                                array('title' => 'Disable', 'value' => ''),
                                array('title' => 'Left-to-Right', 'value' => 'leftToRight'),
                                array('title' => 'Right-to-Left', 'value' => 'rightToLeft'),
                                array('title' => 'Top-to-Bottom', 'value' => 'topToBottom'),
                                array('title' => 'Bottom-to-Top', 'value' => 'bottomToTop'),
                                array('title' => 'Fade In', 'value' => 'fadeIn'),
                                array('title' => 'Zoom', 'value' => 'zoom')
                            ))) ?>
                    </label>
                </div>
            </div>
        </div>
        <div class="wpContentNavConf" data-wp-content-nav-section="freeArea">
            <div class="wpContentNavName">Free Area</div>
            <div class="wpContentNavValue">
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Min width</span>
                        <input type="number" class="form-control" data-wp-content-nav-parameter="minWidth"
                               value="<?php echo @$sections['freeArea']['minWidth'] ?>">
                        <span>PX</span>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Max width</span>
                        <input type="number" class="form-control" data-wp-content-nav-parameter="maxWidth"
                               value="<?php echo @$sections['freeArea']['maxWidth'] ?>">
                        <span>PX</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="wpContentNavConf" data-wp-content-nav-section="compatibility">
            <div class="wpContentNavName">Compatibility</div>
            <div class="wpContentNavValue">
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">
                            Margin top<br>
                            (when fixed)
                        </span>
                        <input type="number" class="form-control" data-wp-content-nav-parameter="marginTop"
                               value="<?php echo @$sections['compatibility']['marginTop'] ?>">
                        <span>PX</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="wpContentNavConf" data-wp-content-nav-section="listStyle">
            <div class="wpContentNavName">List Style</div>
            <div class="wpContentNavValue">
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Numbered</span>
                        <?php echo $h->generateCheckbox(array(
                            'attr' => 'data-wp-content-nav-parameter="numbered"',
                            'value' => &$sections['listStyle']['numbered']
                        )) ?>
                        <span>Enable</span>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Shift inserted</span>
                        <?php echo $h->generateCheckbox(array(
                            'attr' => 'data-wp-content-nav-parameter="shiftInserted"',
                            'value' => &$sections['listStyle']['shiftInserted']
                        )) ?>
                        <span>Enable</span>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Prefix<br>(○ ● ◆ ◇ ✖)</span>
                        <input type="text" class="form-control wpContentNavSmall" data-wp-content-nav-parameter="prefix"
                               value="<?php echo @$sections['listStyle']['prefix'] ?>">
                    </label>
                </div>


            </div>
        </div>
        <div class="wpContentNavConf" data-wp-content-nav-section="itemColors">
            <div class="wpContentNavName">Item Colors</div>
            <div class="wpContentNavValue">
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Default</span>
                        <span class="wpContentNavColorpickerContainer">
                            <span class="input-group colorpicker-component">
                                <input type="text" class="form-control" data-wp-content-nav-parameter="default"
                                       data-wp-content-nav-type="colorpicker"
                                       value="<?php echo @$sections['itemColors']['default'] ?>">
                                <span class="input-group-addon"><i></i></span>
                            </span>
                        </span>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Active</span>
                        <span class="wpContentNavColorpickerContainer">
                            <span class="input-group colorpicker-component">
                                <input type="text" class="form-control" data-wp-content-nav-parameter="active"
                                       data-wp-content-nav-type="colorpicker"
                                       value="<?php echo @$sections['itemColors']['active'] ?>">
                                <span class="input-group-addon"><i></i></span>
                            </span>
                        </span>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Mouse Over</span>
                        <span class="wpContentNavColorpickerContainer">
                            <span class="input-group colorpicker-component">
                                <input type="text" class="form-control" data-wp-content-nav-parameter="mouseOver"
                                       data-wp-content-nav-type="colorpicker"
                                       value="<?php echo @$sections['itemColors']['mouseOver'] ?>">
                                <span class="input-group-addon"><i></i></span>
                            </span>
                        </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="wpContentNavConf" data-wp-content-nav-section="fontDefault">
            <div class="wpContentNavName">Default Fonts</div>
            <div class="wpContentNavValue">
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Size</span>
                        <input type="number" class="form-control" data-wp-content-nav-parameter="size"
                               value="<?php echo @$sections['fontDefault']['size'] ?>">
                        <span>PX</span>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Weight</span>
                        <?php echo $h->generateSelect(array(
                            'attr' => 'class="form-control" data-wp-content-nav-parameter="weight"',
                            'value' => &$sections['fontDefault']['weight'],
                            'options' => array(
                                array('title' => 'Default', 'value' => ''),
                                array('title' => 'Lighter', 'value' => 'lighter'),
                                array('title' => 'Normal', 'value' => 'normal'),
                                array('title' => 'Bold', 'value' => 'bold')
                            ))) ?>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Default Color</span>
                        <span class="wpContentNavColorpickerContainer">
                            <span class="input-group colorpicker-component">
                                <input type="text" class="form-control" data-wp-content-nav-parameter="defaultColor"
                                       data-wp-content-nav-type="colorpicker"
                                       value="<?php echo @$sections['fontDefault']['defaultColor'] ?>">
                                <span class="input-group-addon"><i></i></span>
                            </span>
                        </span>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Mouse Over Color</span>
                        <span class="wpContentNavColorpickerContainer">
                            <span class="input-group colorpicker-component">
                                <input type="text" class="form-control" data-wp-content-nav-parameter="hoverColor"
                                       data-wp-content-nav-type="colorpicker"
                                       value="<?php echo @$sections['fontDefault']['hoverColor'] ?>">
                                <span class="input-group-addon"><i></i></span>
                            </span>
                        </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="wpContentNavConf" data-wp-content-nav-section="fontActive">
            <div class="wpContentNavName">Active Font</div>
            <div class="wpContentNavValue">
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Size</span>
                        <input type="number" class="form-control" data-wp-content-nav-parameter="size"
                               value="<?php echo @$sections['fontActive']['size'] ?>">
                        <span>PX</span>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Weight</span>
                        <?php echo $h->generateSelect(array(
                            'attr' => 'class="form-control" data-wp-content-nav-parameter="weight"',
                            'value' => &$sections['fontActive']['weight'],
                            'options' => array(
                                array('title' => 'Default', 'value' => ''),
                                array('title' => 'Lighter', 'value' => 'lighter'),
                                array('title' => 'Normal', 'value' => 'normal'),
                                array('title' => 'Bold', 'value' => 'bold')
                            ))) ?>
                    </label>
                </div>
                <div class="wpContentNavParameter">
                    <label>
                        <span class="wpContentNavParameterTitle">Color</span>
                        <span class="wpContentNavColorpickerContainer">
                            <span class="input-group colorpicker-component">
                                <input type="text" class="form-control" data-wp-content-nav-type="colorpicker"
                                       data-wp-content-nav-parameter="color"
                                       value="<?php echo @$sections['fontActive']['color'] ?>">
                                <span class="input-group-addon"><i></i></span>
                            </span>
                        </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="wpContentNavContentFooter">
            <div class="wpContentNavBtn wpContentNavBtnPurple wpContentNavBtnResetStyle">Reset</div>
        </div>
    </div>
</div>