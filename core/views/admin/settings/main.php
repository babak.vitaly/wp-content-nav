<?php
//delete_option('wp_content_nav_settings');
//global $WPContentNavInstall;
//$WPContentNavInstall->install();


global $WPContentNavSettings;
$settings = $WPContentNavSettings->get();
$h = new WPContentNavHelper();


if (@$data['integration'] !== true)
{
    ?>
    <div class="wrap wpContentNavSettingsPage">
    <h1>WP Content Nav Settings</h1>
    <?php
}
?>
    <div class="wpContentNavWrapper bootstrap-iso" id="wpContentNavSettings">
        <div class="wpContentNavContent">
            <div class="wpContentNavControl">
                <label>
                    <span>Template</span>
                    <select class="wpContentNavTemplate form-control">
                        <option value="spoiler">Spoiler</option>
                        <option value="list">List</option>
                        <option value="box">Box</option>                        
                    </select>
                </label>
                <div class="wpContentNavBtn wpContentNavBtnPurple pContentNavBtnAddStyle">Add Style</div>
            </div>
            <div class="wpContentNavTemplates wpContentNavTableStyle_1">

                <?php foreach ($settings['templates'] as $templateName => $template)
                {
                    echo "<div class='wpContentNavTemplate' data-wp-content-nav-template='$templateName'>";

                    foreach ($template as $styleID => $style)
                    {
                        $root = array();
                        if (@$style['default'] == 'true') $root['class'] = 'wpContentNavDefault';
                        echo $this->loadViewFile(
                            "admin/settings/templates/$templateName/styleItem",
                            array('data' => $style, 'id' => $styleID, 'root' => $root));
                    }

                    echo
                        '<div class="wpContentNavEmptyMessage">' .
                        'To create a new style, Click to "Add Style" button.' .
                        '</div>';
                    echo '</div>';
                } ?>
            </div>
        </div>
    </div>
<?php if (@$data['integration'] !== true)
{ ?>
    <div class="wpContentNavFooter">
        <div class="wpContentNavBtn wpContentNavBtnPurple" id="wpContentNavBtnSave">Save</div>
    </div>
    </div>
<?php } ?>