<?php
global $wp_registered_sidebars, $WPContentNavSettings, $WPContentNavPostMetaBox;
$h = new WPContentNavHelper();
$settings = $WPContentNavSettings->get();
?>
<div class="bootstrap-iso">
    <div class="wpContentNavSection wpContentNavState">
        <div class="wpContentNavParameter">
            <div class="wpContentNavTitle">Active?</div>
            <div class="wpContentNavContent">
                <?php echo $h->generateCheckbox(
                    array(
                        'attr' => 'name="wpContentNav[state]" data-toggle="toggle" data-size="mini" data-width="48"',
                        'value' => &$data['state']
                    )) ?>
            </div>
        </div>
    </div>
    <div class="wpContentNavSections">
        <div class="wpContentNavSection">
            <div class="wpContentNavParameter">
                <div class="wpContentNavTitle">Style</div>
                <div class="wpContentNavContent">
                    <?php echo $h->generateSelect(
                        array(
                            'attr' => 'class="form-control" name="wpContentNav[style]" id="wpContentNavStyleSelector"',
                            'value' => &$data['style'],
                            'options' => $WPContentNavPostMetaBox->generateStyleSelectorOptions($settings)
                        )) ?>
                </div>
            </div>
        </div>
        <div class="wpContentNavSection">
            <div class="wpContentNavParameter wpContentNavParameterMultiline wpContentNavParameterTags">
                <div class="wpContentNavTitle">Tag include</div>
                <div class="wpContentNavContent">
                    <label>
                        <?php echo $h->generateCheckbox(
                            array(
                                'attr' => 'name="wpContentNav[tags][h1]" value="h1"',
                                'value' => &$data['tags']['h1']
                            )) ?>
                        <span>H1</span>
                    </label>
                    <label>
                        <?php echo $h->generateCheckbox(
                            array(
                                'attr' => 'name="wpContentNav[tags][h2]" value="h2"',
                                'value' => &$data['tags']['h2']
                            )) ?>
                        <span>H2</span>
                    </label>
                    <label>
                        <?php echo $h->generateCheckbox(
                            array(
                                'attr' => 'name="wpContentNav[tags][h3]" value="h3"',
                                'value' => &$data['tags']['h3']
                            )) ?>
                        <span>H3</span>
                    </label>
                    <label>
                        <?php echo $h->generateCheckbox(
                            array(
                                'attr' => 'name="wpContentNav[tags][h4]" value="h4"',
                                'value' => &$data['tags']['h4']
                            )) ?>
                        <span>H4</span>
                    </label>
                    <label>
                        <?php echo $h->generateCheckbox(
                            array(
                                'attr' => 'name="wpContentNav[tags][h5]" value="h5"',
                                'value' => &$data['tags']['h5']
                            )) ?>
                        <span>H5</span>
                    </label>
                    <label>
                        <?php echo $h->generateCheckbox(
                            array(
                                'attr' => 'name="wpContentNav[tags][h6]" value="h6"',
                                'value' => &$data['tags']['h6']
                            )) ?>
                        <span>H6</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="wpContentNavSection wpContentNavSectionArea" data-wp-content-nav-section="spoilerTemplate">
            <div class="wpContentNavParameter">
                <div class="wpContentNavTitle">Parent Tag</div>
                <div class="wpContentNavContent">
                    <?php echo $h->generateSelect(
                        array(
                            'attr' => 'class="form-control" name="wpContentNav[templates][spoiler][parentTag]"',
                            'value' => &$data['templates']['spoiler']['parentTag'],
                            'options' => array(
                                array('title' => 'H1', 'value' => 'h1'),
                                array('title' => 'H2', 'value' => 'h2'),
                                array('title' => 'H3', 'value' => 'h3'),
                                array('title' => 'H4', 'value' => 'h4'),
                                array('title' => 'H5', 'value' => 'h5'),
                            )))
                    ?>
                </div>
            </div>
        </div>
        <div class="wpContentNavSection wpContentNavSectionArea" data-wp-content-nav-section="selector">
            <div class="wpContentNavParameter">
                <div class="wpContentNavTitle">Area</div>
                <div class="wpContentNavContent">
                    <?php echo $h->generateSelect(
                        array(
                            'attr' => 'class="form-control" name="wpContentNav[area][type]"',
                            'value' => &$data['area']['type'],
                            'options' => array(
                                array('title' => 'Sidebar', 'value' => 'sidebar'),
                                array('title' => 'Free', 'value' => 'free'),
                            )))
                    ?>
                </div>
            </div>
        </div>
        <div class="wpContentNavSection wpContentNavSectionArea" data-wp-content-nav-section="sidebar">
            <div class="wpContentNavParameter">
                <div class="wpContentNavTitle">Sidebar Name</div>
                <div class="wpContentNavContent">
                    <?php
                    $options = array();
                    foreach ($wp_registered_sidebars as $sidebar)
                    {
                        $options[] = array('title' => $sidebar['name'], 'value' => $sidebar['id']);
                    }
                    echo $h->generateSelect(
                        array(
                            'attr' => 'class="form-control" name="wpContentNav[area][sidebar][id]"',
                            'value' => &$data['area']['sidebar']['id'],
                            'options' => $options
                        ))
                    ?>
                </div>
            </div>
        </div>
        <div class="wpContentNavSection wpContentNavSectionArea" data-wp-content-nav-section="free">
            <div class="wpContentNavParameter">
                <div class="wpContentNavTitle">Free Position</div>
                <div class="wpContentNavContent">
                    <?php echo $h->generateSelect(
                        array(
                            'attr' => 'class="form-control" name="wpContentNav[area][free][position]"',
                            'value' => &$data['area']['free']['position'],
                            'options' => array(
                                array('title' => 'Right Top', 'value' => 'rightTop'),
                                array('title' => 'Right Middle', 'value' => 'rightMiddle'),
                                array('title' => 'Right Bottom', 'value' => 'rightBottom'),
                                array('title' => 'Left Top', 'value' => 'leftTop'),
                                array('title' => 'Left Middle', 'value' => 'leftMiddle'),
                                array('title' => 'Left Bottom', 'value' => 'leftBottom'),
                            )))
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>