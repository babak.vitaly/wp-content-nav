<?php

class WPContentNavHelper
{
    //return if set
    function rifs(&$str, $template = NULL)
    {
        if (!empty($str))
        {
            if ($template == NULL) return $str;
            return sprintf($template, $str);
        }
        return NULL;
    }
    function generateSelect($args)
    {
        $result = '';
        $result .= '<select ' . @$args['attr'] . '>';
        foreach ($args['options'] as $item)
        {
            $selected = '';
            if (@$item['value'] != '' && @$item['value'] == @$args['value']) $selected = 'selected';
            $result .=
                '<option ' . @$item['attr'] . ' value="' . @$item['value'] . '" ' . $selected . '>' .
                @$item['title'] .
                '</option>';
        }
        $result .= '</select>';
        return $result;
    }

    function generateCheckbox($args)
    {
        $checked = '';
        if (@$args['value'] != '') $checked = 'checked';
        return '<input type="checkbox" ' . @$args['attr'] . ' ' . $checked . '>';
    }

    function generateCompatibleCSSProperty($property, $value)
    {
        $prefixes = array('-webkit-','-ms-', '-moz-', '-o-', '');
        $result = '';
        foreach ($prefixes as $prefix)
        {
            $result .= "$prefix$property:$value;";
        }
        return $result;
    }
}
