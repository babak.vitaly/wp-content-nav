<?php

class WPContentNavCommon
{
    public $pluginVersion = '1.5.0';
    public $pluginName;
    protected $views;

    function loadView()
    {
        $currentViews = $this->views[current_filter()];
        include(dirname(WP_CONTENT_NAV_PLUGIN_ROOT) . "/core/views/$currentViews.php");
    }

    function loadViewFile($fileName, $data = false, $display = false, $fullPath = false)
    {
        if (!$display) ob_start();
        if ($fullPath === false)
        {
            include(dirname(WP_CONTENT_NAV_PLUGIN_ROOT) . "/core/views/$fileName.php");
        } else
        {
            include(dirname(WP_CONTENT_NAV_PLUGIN_ROOT) . "/$fileName.php");
        }
        if (!$display)
        {
            $html = ob_get_contents();
            ob_end_clean();
            return $html;
        }
    }

}