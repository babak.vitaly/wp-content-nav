<?php

class WPContentNavSettingsIntegration extends WPContentNavAdminCommon
{
    function __construct()
    {
        add_filter('wp_cprty_integration_settings_tab_nav', array(&$this, 'tabNavigation'));
        add_action('wp_cprty_integration_settings_tab_panel', array(&$this, 'tabPanel'));
    }

    function tabNavigation($tabs)
    {
        $tabs[] = array('url' => '#wpCprtyWPContentNav', 'name' => 'WP Content Nav');
        return $tabs;
    }

    function tabPanel()
    {
        echo '<div class="bootstrap-iso wpCprtyWrapper wpCprtyWPContentNav">';
        echo $this->loadViewFile("admin/settings/main", array('integration' => true));
        echo '</div>';
    }
}

$WPContentNavSettingsIntegration = new WPContentNavSettingsIntegration();

