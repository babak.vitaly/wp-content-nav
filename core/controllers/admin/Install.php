<?php

class WPContentNavInstall
{
    function __construct()
    {
        register_activation_hook(WP_CONTENT_NAV_PLUGIN_ROOT, array(&$this, 'install'));
    }

    function install()
    {
        $templates = array(
            'box' =>
                array(
                    'default' =>
                        array(
                            'default' => 'true',
                            'name' => 'Default',
                            'sections' =>
                                array(
                                    'effects' =>
                                        array(
                                            'state' => '',
                                        ),
                                    'blockTitle' =>
                                        array(
                                            'text' => 'test',
                                            'iconName' => '',
                                        ),
                                    'itemColors' =>
                                        array(
                                            'default' => '',
                                            'active' => '',
                                            'mouseOver' => '',
                                        ),
                                    'fontDefault' =>
                                        array(
                                            'size' => '',
                                            'weight' => '',
                                            'defaultColor' => '',
                                            'hoverColor' => '',
                                        ),
                                    'fontActive' =>
                                        array(
                                            'size' => '',
                                            'weight' => '',
                                            'color' => '',
                                        ),
                                ),
                        ),
                ),
            'list' =>
                array(
                    'default' =>
                        array(
                            'default' => 'true',
                            'name' => 'Default',
                            'sections' =>
                                array(
                                    'blockTitle' =>
                                        array(
                                            'text' => '',
                                            'iconName' => '',
                                        ),
                                    'listStyle' =>
                                        array(
                                            'numbered' => '',
                                            'shiftInserted' => '',
                                        ),
                                    'effects' =>
                                        array(
                                            'name' => '',
                                        ),
                                    'itemColors' =>
                                        array(
                                            'default' => '',
                                            'active' => '',
                                            'mouseOver' => '',
                                        ),
                                    'fontDefault' =>
                                        array(
                                            'size' => '',
                                            'weight' => '',
                                            'defaultColor' => '',
                                            'hoverColor' => '',
                                        ),
                                    'fontActive' =>
                                        array(
                                            'size' => '',
                                            'weight' => '',
                                            'color' => '',
                                        ),
                                ),
                        ),
                ),
            'spoiler' =>
                array(
                    'default' =>
                        array(
                            'default' => 'true',
                            'name' => 'Default',
                            'sections' =>
                                array(
                                    'blockTitle' =>
                                        array(
                                            'text' => '',
                                            'iconName' => '',
                                        ),
                                    'freeArea' =>
                                        array(
                                            'minWidth' => '',
                                            'maxWidth' => '',
                                        ),
                                    'compatibility' =>
                                        array(
                                            'marginTop' => '',
                                        ),
                                    'parentAndChild' =>
                                        array(
                                            'parentIconName' => '',
                                            'childIconName' => 'fa-info-circle',
                                        ),
                                    'itemColors' =>
                                        array(
                                            'default' => '',
                                            'active' => '',
                                            'mouseOver' => '',
                                        ),
                                    'fontDefault' =>
                                        array(
                                            'size' => '',
                                            'weight' => '',
                                            'defaultColor' => '',
                                            'hoverColor' => '',
                                        ),
                                    'fontActive' =>
                                        array(
                                            'size' => '',
                                            'weight' => '',
                                            'color' => '',
                                        ),
                                ),
                        )
                )
        );
        
        $changed = false;
        $settings = get_option('wp_content_nav_settings');
        if ($settings === false)
        {
            $settings = array(
                'version' => '0',
                'templates' => array()
            );
            $changed = true;
        }

        foreach ($templates as $templateName => $template)
        {
            $settingsTemplate =& $settings['templates'][$templateName];
            if (!isset($settingsTemplate))
            {
                $settingsTemplate = $template;
                $changed = true;
            }
        }

        if ($changed) update_option('wp_content_nav_settings', $settings);

    }
}