<?php

class WPContentNavPostMetaBox extends WPContentNavAdminCommon
{
    public $metaKeys = array(
        'settings' => '_wp_content_nav_settings',
        'tags' => '_wp_content_nav_tags',
    );

    function __construct()
    {
        add_action('add_meta_boxes', array(&$this, 'addMetaBox'));
        add_filter('wp_insert_post_data', array(&$this, 'insertPostData'), 10, 2);
    }

    function loadMetaBox()
    {
        $postID = get_the_ID();
        $settings = get_metadata('post', $postID, $this->metaKeys['settings']);
        if (isset($settings[0])) $settings = $settings[0];
        echo $this->loadViewFile('admin/postMetaBox', $settings);
    }

    function generateStyleSelectorOptions($settings)
    {
        $templates = &$settings['templates'];
        $options = array();
        if (!isset($templates)) return $options;
        foreach ($templates as $templateName => $template)
        {
            foreach ($template as $styleItemID => $styleItem)
            {
                $options[] = array(
                    'title' => ucfirst($templateName) . '->' . ucfirst($styleItem['name']),
                    'value' => "$templateName:{$styleItemID}"
                );
            }
        }
        return $options;
    }

    function getSupportedPostTypes()
    {
        $postTypes = get_post_types(array('public' => true), 'names');
        unset($postTypes['attachment']);
        return $postTypes;
    }

    function addMetaBox()
    {
        add_meta_box(
            'WPContentNavPostMetaBox',
            'WP Content Nav',
            array(&$this, 'loadMetaBox'),
            $this->getSupportedPostTypes(),
            'side',
            'low'
        );
    }

    function isProcessPost($postType, $postID)
    {
        if (!in_array($postType, $this->getSupportedPostTypes())) return false;
        if ('trash' === get_post_status($postID)) return false;
        return true;
    }

    function insertPostData($data, $postarr)
    {
        if (!$this->isProcessPost($postarr['post_type'], $postarr['ID'])) return $data;
        if (!isset($_POST['wpContentNav'])) return $data;
        $settings = $_POST['wpContentNav'];
        $tags = array();
        $slugs = array();
        if (@$settings['state'] === 'on')
        {
            $sourceContent = stripslashes($data['post_content']);
            $domOb = new DOMDocument('4.0');
            $domOb->formatOutput = false;
            $domOb->preserveWhiteSpace = true;

            //@ needs to ignore duplicated ID(name) issue
            @$domOb->loadHTML(
                '<html><head>' .
                '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' .
                '</head><body>' . $sourceContent . '</body></html>'
            );

            $domTagsA = array();
            foreach ($domOb->getElementsByTagName('a') as $item)
            {
                if ($item->getAttribute('data-wp-content-nav') !== 'destination') continue;
                $domTagsA[] = $item;
            }
            foreach ($domTagsA as $item)
            {
                $ps = $item->previousSibling;
                $item->parentNode->removeChild($item);
                $psv = $ps->nodeValue;
                if (substr($psv, -2, 2) == "\r\n") $ps->nodeValue = substr($psv, 0, -2);
            }

            if (isset($settings['tags']))
            {
                $domTagsFiltered = array();
                foreach ($domOb->getElementsByTagName('*') as $item)
                {
                    if (!in_array($item->tagName, $settings['tags'])) continue;
                    $domTagsFiltered[] = $item;
                }

                foreach ($domTagsFiltered as $item)
                {
                    if (strlen($item->textContent) == 0) continue;
                    $slug = $this->getUniqueSlug($item->textContent, $slugs);
                    $a = $domOb->createElement('a');
                    $a->setAttribute('name', $slug);
                    $a->setAttribute('data-wp-content-nav', 'destination');
                    $tags[] = array('tag' => $item->tagName, 'slug' => $slug, 'text' => $item->textContent);
                    $item->parentNode->insertBefore($a, $item);
                    $item->parentNode->insertBefore($domOb->createTextNode("\r\n"), $item);
                }
            }

            preg_match("/<body[^>]*>(.*?)<\/body>/is", $domOb->saveHTML(), $matches);
            $data['post_content'] = addslashes($matches[1]);
        }
        update_metadata('post', $postarr['ID'], $this->metaKeys['tags'], $tags);
        update_metadata('post', $postarr['ID'], $this->metaKeys['settings'], $settings);
        return $data;
    }
}

$WPContentNavPostMetaBox = new WPContentNavPostMetaBox();