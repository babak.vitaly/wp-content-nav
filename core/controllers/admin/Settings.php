<?php

class WPContentNavSettings extends WPContentNavAdminCommon
{
    public $optionName = 'wp_content_nav_settings';
    private $stored = 'no';

    function __construct()
    {
        add_action('admin_enqueue_scripts', array(&$this, 'loadJS'));
        add_action('wp_ajax_wpContentNavSettings', array(&$this, 'save'));
    }

    function get()
    {
        if ($this->stored === 'no') $this->stored = get_option($this->optionName);
        return $this->stored;
    }

    function save()
    {
        $settings = &$_POST['data'];
        if (!isset($settings)) die(json_encode(false));
        update_option($this->optionName, $settings);
        die(json_encode(true));
    }

    function isClassPage()
    {
        return true;
    }

    function loadJS()
    {
        if (!$this->isClassPage()) return;
        $phpVars = array('templates' =>
            array(
                'box' => array('styleItem' => $this->loadViewFile('admin/settings/templates/box/styleItem')),
                'list' => array('styleItem' => $this->loadViewFile('admin/settings/templates/list/styleItem')),
                'spoiler' => array('styleItem' => $this->loadViewFile('admin/settings/templates/spoiler/styleItem'))
            )
        );
        wp_localize_script('wp-content-nav-admin-common', 'WPContentNavPhpVarsSettings', $phpVars);
    }
}

$WPContentNavSettings = new WPContentNavSettings();