<?php

class WPContentNavVisitorsCommon extends WPContentNavCommon
{
    function __construct()
    {
        add_action('wp_print_styles', array(&$this, 'loadCSS'));
        add_action('wp_enqueue_scripts', array(&$this, 'loadJS'));
        add_action('widgets_init', array(&$this, 'registerWidget'));
        add_action('wp_footer', array(&$this, 'footer'), 10);
    }

    function footer()
    {
        $this->printDynamicCSS();
    }

    function registerWidget()
    {
        register_widget('WPContentNavBoxWidget');
    }

    function printDynamicCSS()
    {
        global $WPContentNavNavigation;
        global $WPContentNavSettings;
        $Nav =& $WPContentNavNavigation;

        if ($Nav->state['state'] !== 'enable') return;
        if (empty($Nav->template) || empty($Nav->styleID)) return;
        
        $templateSettings = $WPContentNavSettings->get();
        $style =& $templateSettings['templates'][$Nav->template][$Nav->styleID];

        if (!isset($style)) return;
        echo "<style id='wpContenNavDynamicCss'>" . $this->loadViewFile("visitors/css/$Nav->template", $style) . "</style>";
    }

    function loadCSS()
    {
        global $WPContentNavNavigation;
        $Nav =& $WPContentNavNavigation;
        if ($Nav->state['state'] !== 'enable') return;

        wp_enqueue_style(
            'font-awesome',
            WP_CONTENT_NAV_PLUGIN_ROOT_URL . '/css/font-awesome.min.css',
            array(),
            $this->pluginVersion
        );

        wp_enqueue_style(
            'wp-content-nav-visitors',
            WP_CONTENT_NAV_PLUGIN_ROOT_URL . '/css/visitors.css',
            array(),
            $this->pluginVersion
        );
    }

    function loadJS()
    {
        global $WPContentNavNavigation;
        $Nav =& $WPContentNavNavigation;
        if ($Nav->state['state'] !== 'enable') return;

        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-effects-core');
        wp_enqueue_script(
            'wp-content-nav-visitors-common',
            WP_CONTENT_NAV_PLUGIN_ROOT_URL . '/js/visitors.js',
            array(),
            $this->pluginVersion
        );
        //  var_dump($Nav->style);
        $phpVars = array('style' => $Nav->style);
        $p =& $phpVars['style']['sections']['compatibility']['marginTop'];
        if (@$p == '') $p = 0;
        wp_localize_script('wp-content-nav-visitors-common', 'wpContentNavPhpVars', $phpVars);
    }
}

$WPContentNavVisitorsCommon = new WPContentNavVisitorsCommon();