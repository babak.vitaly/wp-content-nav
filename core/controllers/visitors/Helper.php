<?php

class WPContentNavVisitorsHelper extends WPContentNavHelper
{
    function getTagNum($tag)
    {
        return substr($tag, -1, 1);
    }

    function generateList(&$postTags, $parameters = array())
    {
        if (sizeof($postTags) == 0) return;

        echo "<ul>";
        echo "<li class='wpContentNavParent'>";
        $li = 0;
        $ul = 0;
        $prevTag = $postTags[0];
        $num[0] = $num[1] = $num[2] = $num[3] = $num[4] = $num[5] = 0;
        $childIcon = $this->rifs($parameters['childIconName'], '<i class="fa %s"></i>');

        foreach ($postTags as $key => $postTag)
        {
            $tagDiff = $this->getTagNum($prevTag['tag']) - $this->getTagNum($postTag['tag']);
            $childIconTmp = '';
            if ($tagDiff < 0)
            {
                $tagDiffAbs = abs($tagDiff);
                while ($tagDiffAbs)
                {
                    echo "<ul>";
                    $ul++;
                    echo "<li class='wpContentNavChild'>";
                    $li++;
                    $tagDiffAbs--;
                    $num[$li] = 0;
                }
            }

            if ($tagDiff > 0)
            {
                while ($tagDiff)
                {
                    if ($li > 0)
                    {
                        echo "</li>";
                        $li--;
                    }
                    if ($ul > 0)
                    {
                        echo "</ul>";
                        $ul--;
                    }
                    $tagDiff--;
                }
            }
            if ($tagDiff == 0 && $key > 0)
            {
                $liClass = 'wpContentNavParent';
                if($li > 0)$liClass = 'wpContentNavChild';
                echo "</li>";
                echo "<li class='$liClass'>";
                $num[$li]++;
            }

            if ($li > 0)
            {
                $childIconTmp = $childIcon;
            }

            $numText = '';
            for ($i = 0; $i <= $li; $i++)
            {
                $numText .= ($num[$i] + 1) . ".";
            }

            echo
                "<a href='#{$postTag['slug']}'>" .
                "<span class='wpContentNavNumber'>{$numText} </span>" .
                $childIconTmp .
                "<span class='wpContentNavTitle'>{$postTag['text']}</span>" .
                "</a>";

            $prevTag = $postTag;
        }

        while ($li || $ul)
        {
            if ($li > 0)
            {
                echo "</li>";
                $li--;
            }
            if ($ul > 0)
            {
                echo "</ul>";
                $ul--;
            }
        }
        echo "</ul>";
    }
}