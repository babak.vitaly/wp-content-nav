<?php

class WPContentNavNavigation extends WPContentNavCommon
{
    public $state = array('state' => 'disable', 'message' => '');
    public $postSettings, $postTags, $template, $style, $styleVersion, $styleID;

    function __construct()
    {
        add_action('template_redirect', array(&$this, 'init'));
        add_action('wp_footer', array(&$this, 'free'));
    }

    function free()
    {
        if ($this->state['state'] !== 'enable') return;
        $data['this'] = $this;
        $data['root']['attr'] = "data-wp-content-nav-area-position='rightMiddle'";
        if ($this->postSettings['area']['type'] === 'free')
        {
            $data['body']['html'] = $this->generate();
            $data['root']['attr'] =
                "data-wp-content-nav-area-position='{$this->postSettings['area']['free']['position']}'";
        } else
        {
            $data['root']['class'] = 'wpContentNavDisabled wpContentNavAreaContentHide';
        }
        echo $this->loadViewFile("visitors/areas/free", $data);
    }

    function generate()
    {
        if ($this->state['state'] === 'error') return $this->state['message'];
        $data = array('this' => $this, 'root' => array('class' => '', 'attr' => ''));
        switch ($this->template)
        {
            case 'list':
                if (@$this->style['sections']['listStyle']['numbered'] === 'on')
                    $data['root']['class'] .= 'wpContentNavListStyleNumbered ';
                if (@$this->style['sections']['listStyle']['shiftInserted'] === 'on')
                    $data['root']['class'] .= 'wpContentNavListStyleShiftInserted ';

                $data['root']['attr'] .=
                    'data-wp-content-nav-effect="' .
                    @$this->style['sections']['effects']['name'] . '" ';
                break;
            case 'box':

                $data['root']['attr'] .=
                    'data-wp-content-nav-effect="' .
                    @$this->style['sections']['effects']['name'] . '" ';
                break;
        }
        return $this->loadViewFile("visitors/templates/$this->template", $data);
    }

    function init()
    {
        global $WPContentNavPostMetaBox, $WPContentNavSettings;
        if (!is_page() && !is_single()) return;
        $postID = get_the_ID();
        $this->postSettings = get_metadata('post', $postID, $WPContentNavPostMetaBox->metaKeys['settings']);
        if (!isset($this->postSettings[0])) return;
        $this->postSettings = $this->postSettings[0];
        if (@$this->postSettings['state'] !== 'on') return;
        if (sizeof($this->postSettings['tags']) == 0) return;

        $this->postTags = get_metadata('post', $postID, $WPContentNavPostMetaBox->metaKeys['tags']);
        if (!isset($this->postTags[0])) return;
        $this->postTags = $this->postTags[0];
        if (sizeof($this->postTags) == 0) return;
        $templateSettings = $WPContentNavSettings->get();

        $ts = explode(':', $this->postSettings['style']);
        $this->template = $ts[0];
        $this->styleID = $ts[1];
         $this->style =& $templateSettings['templates'][$this->template][$this->styleID];
        if (!isset($this->style))
        {
            $this->styleID = 'default';
            $this->style = $templateSettings['templates'][$this->template][$this->styleID];
        }
        $this->styleVersion =& $templateSettings['version'];

        $p = &$this->style['sections']['blockTitle']['iconName'];
        if (@$p == '') $p = 'fa-list-ol';
        $this->state['state'] = 'enable';
    }

}

$WPContentNavNavigation = new WPContentNavNavigation();