<?php

class WPContentNavBoxWidget extends WP_Widget
{
    function __construct()
    {
        $widget_ops = array(
            'classname' => 'wpContentNavWidget',
            'description' => 'WP Content Nav Widget'
        );
        $control_ops = array('width' => 300, 'height' => 350, 'id_base' => 'wp-content-nav-widget');
        parent::__construct('wp-content-nav-widget', 'WP Content Nav', $widget_ops, $control_ops);
    }

    function widget($args, $instance)
    {
        global $WPContentNavNavigation, $WPContentNavAdminCommon;
        $navigation =& $WPContentNavNavigation;
        if ($navigation->state['state'] === 'disable') return;
        if ($navigation->postSettings['area']['type'] !== 'sidebar') return;
        if ($args['id'] != $navigation->postSettings['area']['sidebar']['id']) return;
        $title = apply_filters('widget_title', $instance['title']);
        echo $args['before_widget'];
        if ($title != "") echo $args['before_title'] . $title . $args['after_title'];
        $WPContentNavAdminCommon->loadViewFile("visitors/areas/sidebar", $WPContentNavNavigation->generate(), true);
        echo $args['after_widget'];
        $this->displayed = true;
    }

    function update($newInstance, $oldInstance)
    {
        return $newInstance;
    }

    function form($instance)
    {
        global $WPContentNavAdminCommon;
        $defaults = array('title' => 'Nav menu');
        $data['instance'] = wp_parse_args((array)$instance, $defaults);
        $data['wpWidget'] = $this;
        echo $WPContentNavAdminCommon->loadViewFile('admin/widget', $data);
    }
}