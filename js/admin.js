(function ($)
{
    var it = window.WPContentNavCommonAdmin = {
        serverDate: {
            browserLoad: 0,
            init: function ()
            {
                var d = new Date();
                it.serverDate.browserLoad = d.getTime();
            },
            get: function ()
            {
                var d = new Date();
                var browserWorked = d.getTime() - it.serverDate.browserLoad;
                return WPContentNavPhpVarsCommon.date + browserWorked;
            }
        },
        generateUniqueName: function (names, name)
        {
            var uniqueName = name;
            for (var i = 1; i < 1000; i++)
            {
                if ($.inArray(uniqueName, names) === -1)break;
                uniqueName = name + ' ' + i;
            }
            return uniqueName;
        },
        settingError: {
            box: false,
            show: function (message)
            {
                if (it.settingError.box !== false)$(it.settingError.box).remove();
                it.settingError.box = $(WPContentNavPhpVarsCommon.views.settingsErrors).prependTo('.wrap');
                $('strong', it.settingError.box).html(message);
                var $dismiss = it.settingError.box;
                $dismiss.on('click', function ()
                    {
                        var $wpContentNavSettingError = $(this).closest('.wpContentNavSettingError');
                        $wpContentNavSettingError.remove();
                    }
                );
            }
        },
        loading: {
            object: false,
            init: function ()
            {
                if (it.loading.object !== false)return;
                it.loading.object = $(WPContentNavPhpVarsCommon.views.loading).appendTo('body');
            },
            show: function ()
            {
                it.loading.init();
                $(it.loading.object).show();
            },
            hide: function ()
            {
                it.loading.init();
                $(it.loading.object).hide();
            }
        },
        ajaxRequest: function (param, cb)
        {
            var postData = $.param(param);
            $.ajax({
                    url: WPContentNavPhpVarsCommon.handlerURL,
                    type: "POST",
                    data: postData,
                    dataType: "json"
                }
            ).done(function (data)
                {
                    cb(data);
                }
            );
        },
        getRealFieldValue: function ($field)
        {
            var value = '';
            var type = $field.attr('type');
            if (type === 'checkbox')
            {
                if ($field.prop('checked'))value = 'on';
            } else
            {
                value = $field.val();
            }
            return value;
        },
        iconBox: {
            update: function ($root, $iconNameInput)
            {
                var $preview = $root.find('.wpContentNavIconPreview i');
                var value = $iconNameInput.val();
                $preview.attr('class', 'fa ' + value);
            },
            init: function ($elements)
            {
                $.each($elements, function ()
                {
                    var $element = $(this);
                    var $iconNameInput = $element.find('input[type="text"]');
                    it.iconBox.update($element, $iconNameInput);
                    $iconNameInput.on('change', function ()
                        {
                            var $iconNameInput = $(this);
                            var $root = $iconNameInput.closest('.wpContentNavIconBox');
                            it.iconBox.update($root, $iconNameInput);
                        }
                    );
                });
            }
        },
        editableTitle: function ($edit, action)
        {
            $edit.on('click', function ()
                {
                    var $root = $(this).closest('.wpContentNavEditableTitle');
                    var $title = $('.wpContentNavEditableTitleText', $root);
                    var $editButton = $('.wpContentNavEditableTitleEdit', $root);

                    var maxLength = $root.attr('data-wp-cprty-max-length');
                    if (maxLength === undefined) maxLength = 20;

                    $title.css('display', 'none');
                    $editButton.css('display', 'none');
                    var inputHTML = '<input type="text" class="form-control wpContentNavHiddenEdit" maxlength="' + maxLength + '">';
                    var $input = $(inputHTML).insertAfter($title);
                    $input.focus().val($title.html().trim());
                    if (action !== undefined)action($root, 'activate');

                    function setValue(input)
                    {
                        var value = input.value;
                        $(input).remove();
                        $title.css('display', '').html(value);
                        $editButton.css('display', '');
                        if (action !== undefined)action($root, 'done', value);
                    }

                    $input.on('keypress', function (e)
                        {
                            if (e.which == 13)
                            {
                                setValue(this);
                                return false;
                            }
                        }
                    ).on('blur', function ()
                        {
                            setValue(this);
                        }
                    );

                }
            );
        }
    };

})(jQuery);

(function ($)
{
    var it = window.WPContentNavPostMetaBox = {
        sel: {area: {}, mainToggle: {}},
        area: {
            set: function (value)
            {
                var sel = it.sel;
                sel.area.$areas.css('display', '');
                var $section = sel.area.$areas.filter('[data-wp-content-nav-section="' + value + '"]');
                $section.css('display', 'block');
            },
            init: function ()
            {
                var sel = it.sel;
                sel.area.$areas = sel.$root.find('.wpContentNavSectionArea');
                sel.area.$areaSelector =
                    sel.area.$areas
                        .filter('[data-wp-content-nav-section="selector"]')
                        .find('select');

                sel.area.$areaSelector.on('change', function ()
                {
                    it.area.set(this.value);
                });
                it.area.set(sel.area.$areaSelector.val());
            }
        },
        mainToggle: {
            set: function (state)
            {
                var sel = it.sel;
                var display = '';
                if (state)display = 'block';
                sel.mainToggle.$root.css('display', display);
            },
            init: function ()
            {
                var sel = it.sel;
                sel.mainToggle.$root = sel.$root.find('.wpContentNavSections');
                sel.mainToggle.$toggle = sel.$root.find('.wpContentNavState input');
                sel.mainToggle.$toggle.on('change', function ()
                {
                    it.mainToggle.set($(this).prop('checked'));
                });
                it.mainToggle.set(sel.mainToggle.$toggle.prop('checked'));
            }
        },
        updateStyle: function ($input)
        {
            it.sel.$root.attr('data-wp-content-nav-style', $input.val());
        },
        init: function ($root)
        {
            var sel = it.sel;
            sel.$root = $root;
            it.area.init();
            it.mainToggle.init();
            var $styleSelector = $('#wpContentNavStyleSelector');

            it.updateStyle($styleSelector);
            $styleSelector.on('change', function ()
            {
                it.updateStyle($(this));
            });
        }
    };

    $(document).ready(function ()
    {
        var $selector = $('#WPContentNavPostMetaBox');
        if ($selector.length)it.init($selector);
    });

})(jQuery);


(function ($)
{
    var common = WPContentNavCommonAdmin;
    var it = window.WPContentNavSettings = {
        sel: {control: {}, templates: {}},
        styleItemCountWasChanged: function ($templates)
        {
            $.each($templates, function ()
            {
                var $template = $(this);
                var $styleItems = $template.find('.wpContentNavRowItem');
                var emptyClass = 'wpContentNavTemplateIsEmpty';
                if ($styleItems.length === 0)$template.addClass(emptyClass);
                else $template.removeClass(emptyClass);
            });
        },
        save: function (cb)
        {
            var sel = it.sel;
            sel.templates.$all = sel.templates.$root.find('.wpContentNavTemplate');
            var result = {};

            $.each(sel.templates.$all, function ()
            {
                var $template = $(this);
                var templateName = $template.attr('data-wp-content-nav-template');
                var $styleItems = $template.find('.wpContentNavRowItem');

                $.each($styleItems, function ()
                {
                    var $styleItem = $(this);
                    var $parameterSections = $styleItem.find('.wpContentNavConf');
                    var styleItemName = $styleItem.find('.wpContentNavEditableTitleText').html();
                    var styleItemID = $styleItem.attr('data-wp-content-nav-id');
                    var styleItemDefault = $styleItem.hasClass('wpContentNavDefault');

                    $.each($parameterSections, function ()
                    {
                        var $parameterSection = $(this);
                        var confSectionName = $parameterSection.attr('data-wp-content-nav-section');
                        var $parameters = $parameterSection.find('[data-wp-content-nav-parameter]');

                        $.each($parameters, function ()
                        {
                            var $parameter = $(this);
                            var parameterName = $parameter.attr('data-wp-content-nav-parameter');

                            var value = WPContentNavCommonAdmin.getRealFieldValue($parameter);
                            var jsonSting =
                                '{"version":' + common.serverDate.get() + ',' +
                                '"templates":{' +
                                '"' + templateName + '": {' +
                                '"' + styleItemID + '": {' +
                                '"default": "' + styleItemDefault + '",' +
                                '"name": "' + styleItemName + '",' +
                                '"sections":{' +
                                '"' + confSectionName + '":{' +
                                '"' + parameterName + '": "' + value + '"' +
                                '}}}}}}';
                            $.extend(true, result, JSON.parse(jsonSting));
                        });
                    });
                });
            });
            console.log(result);

            WPContentNavCommonAdmin.ajaxRequest({action: 'wpContentNavSettings', data: result}, function (data)
            {
                cb(data);
            });
        },
        template: {
            update: function ()
            {
                var sel = it.sel;
                var template = sel.control.$tepmlateSelector.val();
                if (sel.templates.$current !== undefined)sel.templates.$current.css('display', '');
                sel.templates.$current =
                    sel.templates.$all.filter('[data-wp-content-nav-template="' + template + '"]');
                sel.templates.$current.css('display', 'block');
            },
            init: function ()
            {
                var sel = it.sel;
                sel.control.$tepmlateSelector.on('change', it.template.update);
                it.template.update();
            }
        },
        styleItem: {
            getRealFieldType: function ($field)
            {
                var dataType = $field.attr('data-wp-content-nav-type');
                if (dataType !== undefined)return dataType;
                var tagName = $field.prop('tagName');
                if (tagName === 'SELECT')return 'select';
                var type = $field.attr('type');
                return type;
            },
            updateColorpicker: function ($input, value)
            {
                var $colorpicker = $input.closest('.colorpicker-component');
                $colorpicker.colorpicker('setValue', value);
                if (value == 'transparent')value = '';
                $input.val(value);
            },
            reset: function ($rowItem)
            {
                var $inputs = $rowItem.find('[data-wp-content-nav-parameter]');
                $.each($inputs, function ()
                    {
                        var $field = $(this);
                        var type = it.styleItem.getRealFieldType($field);
                        switch (type)
                        {
                            case 'select':
                                $field.find('option').prop('selected', false);
                                break;
                            case 'checkbox':
                                $field.prop('checked', false);
                                break;
                            case 'colorpicker':
                                it.styleItem.updateColorpicker($field, 'transparent');
                                break;
                            case 'iconBox':
                                var $iconBox = $field.closest('.wpContentNavIconBox');
                                var $preview = $iconBox.find('.wpContentNavIconPreview i');
                                $field.val('');
                                $preview.attr('class', 'fa ');
                                break;
                            default:
                                $field.val('');
                                break;
                        }
                    }
                );
            },
            init: function ($root, source)
            {
                var sel = it.sel;
                var $btnDetail = $root.find('.wpContentNavBtnDetail');
                var $btnRemove = $root.find('.wpContentNavBtnActionRemove');
                var $editableTitle = $root.find('.wpContentNavEditableTitleEdit');
                var $iconBox = $root.find('.wpContentNavIconBox');
                var $reset = $root.find('.wpContentNavBtnResetStyle');

                $reset.on('click', function ()
                {
                    if (!confirm('Do you really want to reset all fields in this style?'))return;
                    var $rowItem = $(this).closest('.wpContentNavRowItem');
                    it.styleItem.reset($rowItem);
                });

                $btnDetail.on('click', function ()
                {
                    var $rowItem = $(this).closest('.wpContentNavRowItem');
                    $rowItem.toggleClass('wpContentNavClosed');
                });
                $btnRemove.on('click', function ()
                {
                    var $rowItem = $(this).closest('.wpContentNavRowItem');
                    if ($rowItem.hasClass('wpContentNavDefault'))
                    {
                        if (confirm(
                                "You can't remove default style. " +
                                "But you may reset this style. \r\n" +
                                "Do you really want to reset all fields in this style?"
                            ))it.styleItem.reset($rowItem);
                        return;
                    }
                    if (!confirm('Do you really want to remove this style?'))return;
                    $rowItem.remove();
                    it.styleItemCountWasChanged(sel.templates.$current);
                });

                $('.colorpicker-component', $root).colorpicker({
                        container: true
                    }
                );

                WPContentNavCommonAdmin.editableTitle($editableTitle);
                if (source === 'button')$editableTitle.trigger("click");

                WPContentNavCommonAdmin.iconBox.init($iconBox);
            }
        },
        init: function ($root)
        {
            var sel = it.sel;
            var phpVars = WPContentNavPhpVarsSettings;
            sel.$root = $root;
            sel.control.$root = sel.$root.find('.wpContentNavControl');
            sel.control.$tepmlateSelector = sel.control.$root.find('.wpContentNavTemplate');
            sel.control.$addStyle = sel.control.$root.find('.pContentNavBtnAddStyle');
            sel.templates.$root = sel.$root.find('.wpContentNavTemplates');
            sel.templates.$all = sel.templates.$root.find('.wpContentNavTemplate');
            var $styleItems = sel.templates.$all.find('.wpContentNavRowItem');
            sel.$btnSave = $('#wpContentNavBtnSave');

            it.template.init();
            it.styleItem.init($styleItems);
            it.styleItemCountWasChanged(sel.templates.$all);

            sel.control.$addStyle.on('click', function ()
            {
                var templateName = sel.templates.$current.attr('data-wp-content-nav-template');
                var $allStyleItems = sel.templates.$current.find('.wpContentNavRowItem');
                var $newStyleItem = $(phpVars.templates[templateName].styleItem).appendTo(sel.templates.$current);
                var $titleText = $newStyleItem.find('.wpContentNavEditableTitleText');
                var $allTitleText = $allStyleItems.find('.wpContentNavEditableTitleText');

                var allTitleText = [];
                $.each($allTitleText, function (key)
                {
                    allTitleText[key] = $(this).html();
                });
                $titleText.html(WPContentNavCommonAdmin.generateUniqueName(allTitleText, 'New Style'));

                $allStyleItems.addClass('wpContentNavClosed');
                $newStyleItem.removeClass('wpContentNavClosed');
                $newStyleItem.attr('data-wp-content-nav-id', common.serverDate.get());
                it.styleItem.init($newStyleItem, 'button');
                it.styleItemCountWasChanged(sel.templates.$current);
            });

            sel.$btnSave.on('click', function ()
            {
                WPContentNavCommonAdmin.loading.show();
                WPContentNavCommonAdmin.settingError.show('Settings was saved.');
                it.save(function (state)
                {
                    WPContentNavCommonAdmin.loading.hide();
                });
            });

            if (window.WPCprtySettings !== undefined)
            {
                WPCprtySettings.integration.saveQueue.push(function (cb)
                {
                    it.save(cb);
                });
            }

            WPContentNavCommonAdmin.serverDate.init();
        }
    };

    $(document).ready(function ()
    {
        var $selector = $('#wpContentNavSettings');
        if ($selector.length)it.init($selector);
    });

})(jQuery);