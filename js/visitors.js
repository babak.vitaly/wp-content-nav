(function ($)
{
    var configuration = {
        desktopWidth: 960
    };
    var it = window.WPContentNavVisitors = {

        sel: {areas: {free: {}, widget: {}}, navigation: {}},
        phpVars: window.wpContentNavPhpVars,
        common: {
            onHeightChange: function ($element, cb)
            {
                var heightOld = $element.height();
                var heightCurrent;

                setInterval(function ()
                {
                    heightCurrent = $element.height();
                    if (heightOld !== heightCurrent) cb(heightCurrent);
                    heightOld = heightCurrent;
                }, 200);
            },
            sticky: {
                elements: [],
                offsets: [],
                fixed: [],
                minWidth: configuration.desktopWidth,
                overflowOffsets: [],
                limitsActive: [],
                isDesktop: function ()
                {
                    var windowWidth = $(window).width();
                    return windowWidth > it.common.sticky.minWidth;
                },
                resize: function ()
                {
                    if (it.common.sticky.elements.length === 0) return;
                    $.each(it.common.sticky.elements, function (key)
                        {
                            var stickyAlways = $(this).hasClass('wpContentNavStickyAlways');
                            if (stickyAlways || it.common.sticky.isDesktop())
                            {
                                var display = $(this).css('display');
                                var fixed = $(this).hasClass('wpContentNavFixed');
                                if (display === 'none')
                                {
                                    $(this).css('visibility', 'hidden').css('display', 'block');
                                }
                                if (fixed) $(this).removeClass('wpContentNavFixed');
                                it.common.sticky.offsets[key] = $(this).offset();
                                var w = $(this).parent().width();
                                if (fixed) $(this).addClass('wpContentNavFixed');

                                $(this).width(w);

                                if (display === 'none')
                                {
                                    $(this).css('display', 'none');
                                    $(this).css('visibility', 'visible');
                                }
                            } else $(this).css('width', '');
                        }
                    );
                    it.common.sticky.scroll();
                },
                scroll: function ()
                {
                    if (!it.common.sticky.isDesktop()) return;
                    if (it.common.sticky.elements.length === 0) return;
                    var topWindow = $(window).scrollTop();
                    var windowWidth = $(window).width();
                    $.each(it.common.sticky.elements, function (key)
                        {
                            var stickyAlways = $(this).hasClass('wpContentNavStickyAlways');
                            var stickWrapperLimits = $(this).hasClass('wpContentNavStickyWrapperLimits');

                            var wrapperLimitsActive = it.common.sticky.limitsActive[key];
                            if (wrapperLimitsActive === undefined) wrapperLimitsActive = false;
                            if (stickWrapperLimits)
                            {
                                var $wrapper = $(this).parent();
                                var blockHeight = $(this).outerHeight();
                                var clearWrapperOffsetTop = $wrapper.offset().top;
                                if (!it.common.sticky.fixed[key]) clearWrapperOffsetTop -= blockHeight;
                                var wrapperBottomOffset = $wrapper.height() + clearWrapperOffsetTop;
                                var windowOffset = topWindow + blockHeight;
                                if (wrapperBottomOffset < windowOffset) it.common.sticky.limitsActive = wrapperLimitsActive = true;
                                if (wrapperBottomOffset > windowOffset) it.common.sticky.limitsActive = wrapperLimitsActive = false;
                            }

                            if (
                                wrapperLimitsActive === false &&
                                topWindow > it.common.sticky.offsets[key].top &&
                                (windowWidth > it.common.sticky.minWidth || stickyAlways)
                            )
                            {
                                if (!it.common.sticky.fixed[key])
                                {
                                    $(this).addClass('wpContentNavFixed');
                                    it.common.sticky.fixed[key] = true;
                                }
                            } else
                            {
                                if (it.common.sticky.fixed[key])
                                {
                                    $(this).removeClass('wpContentNavFixed');
                                    it.common.sticky.fixed[key] = false;
                                }
                            }
                        }
                    );
                },
                init: function ($elements)
                {
                    it.common.sticky.elements = $elements;
                    $.each(it.common.sticky.elements, function (key)
                        {
                            it.common.sticky.fixed[key] = false;
                        }
                    );
                    it.common.sticky.resize();
                }
            },
            screenType: {
                current: false,
                handlers: [],
                detect: function ()
                {
                    var windowWidth = $(window).width();
                    if (windowWidth > configuration.desktopWidth)
                    {
                        if (it.common.screenType.current !== 'desktop')
                        {
                            it.common.screenType.current = 'desktop';
                            it.common.screenType.onChange(it.common.screenType.current);
                        }
                    }
                    else
                    {
                        if (it.common.screenType.current !== 'mobile')
                        {
                            it.common.screenType.current = 'mobile';
                            it.common.screenType.onChange(it.common.screenType.current);
                        }
                    }
                },
                onChange: function (screenType)
                {
                    $.each(it.common.screenType.handlers, function ()
                    {
                        this(screenType);
                    });
                },
                init: function ()
                {
                    it.common.screenType.detect();
                    $(window).resize(function ()
                    {
                        it.common.screenType.detect();
                    });
                }
            }
        },
        navigation: {
            anchorsOffset: [],
            limiter: {
                enabled: false,
                active: false,
                top: 0,
                bottom: 0
            },
            navButtonClicked: false,
            scrolling: false,
            widgetHeight: 0,
            updateAnchorOffsets: function ()
            {
                var sel = it.sel.navigation;
                $.each(sel.$anchors, function (key)
                {
                    var $anchor = $(this);
                    it.navigation.anchorsOffset[key] = $anchor.offset().top;
                });
            },
            updateLimiterOffsets: function ()
            {
                if (!it.navigation.limiter.enabled) return;

                var sel = it.sel.navigation;
                it.navigation.limiter.top = sel.$limiter.offset().top;
                it.navigation.limiter.bottom = it.navigation.limiter.top + sel.$limiter.height();
            },
            scroll: function ()
            {
                var sel = it.sel.navigation;
                var windowTop = $(window).scrollTop();
                var anchorsOffset = it.navigation.anchorsOffset;
                var marginTop = it.phpVars.style.sections.compatibility.marginTop;
                if (windowTop < anchorsOffset[0])
                {

                    sel.$controls.add(sel.$parents).removeClass('wpContentNavActive');
                    it.free.updateMarginTop();
                }
                $.each(anchorsOffset, function (anchorKey, anchorOffset)
                {
                    var nextAnchorOffset = anchorsOffset[anchorKey + 1];
                    if (windowTop >= anchorOffset - 20 - marginTop &&
                        (windowTop < nextAnchorOffset - 20 - marginTop || nextAnchorOffset === undefined))
                    {
                        sel.$controls.add(sel.$parents).removeClass('wpContentNavActive');
                        var $currentControl = sel.$controls.eq(anchorKey);
                        $currentControl.addClass('wpContentNavActive');
                        $currentControl.closest('.wpContentNavParent').addClass('wpContentNavActive');
                        it.free.updateMarginTop();
                    }
                });

                if (it.navigation.limiter.enabled)
                {
                    if (it.sel.areas.widget.$root.is(":visible"))
                    {
                        it.navigation.widgetHeight = it.sel.areas.widget.$root.outerHeight(true);
                    }

                    if (windowTop > it.navigation.limiter.top && windowTop < it.navigation.limiter.bottom - it.navigation.widgetHeight)
                    {
                        if (it.navigation.limiter.active)
                        {
                            it.sel.areas.widget.$root.css('top', 0);
                            it.navigation.limiter.active = false;
                        }
                    } else
                    {
                        if (windowTop > it.navigation.limiter.bottom - it.navigation.widgetHeight)
                        {
                            it.sel.areas.widget.$root.css('top', it.navigation.limiter.bottom - windowTop - it.navigation.widgetHeight);
                        } else
                        {
                            it.sel.areas.widget.$root.css('top', it.navigation.limiter.top - windowTop);
                        }
                        it.navigation.limiter.active = true;
                    }
                }
            },
            detectAlign: function ()
            {
                var sel = it.sel;
                var windowCenter = $(window).width() / 2;
                var $contentNav = sel.navigation.$root.closest('.wpContentNav');
                var offset = $contentNav.offset().left;


                if (offset < windowCenter)
                {
                    sel.navigation.$root
                        .add($contentNav)
                        .add(sel.areas.widget.$root)
                        .addClass('wpContentNavAlignLeft');
                }
                else
                {
                    sel.navigation.$root
                        .add($contentNav)
                        .add(sel.areas.widget.$root)
                        .removeClass('wpContentNavAlignLeft');
                }
            },
            init: function ($root)
            {
                var sel = it.sel.navigation;
                var marginTop = it.phpVars.style.sections.compatibility.marginTop;
                sel.$anchors = $('[data-wp-content-nav="destination"]');
                sel.$firstAnchor = sel.$anchors.eq(0);
                sel.$controls = $root.find('a');
                sel.$parents = $root.find('.wpContentNavParent');
                sel.$limiter = $('.wpContentNavContentLimiter').eq(0);
                if (sel.$limiter.length !== 0)
                {
                    it.navigation.limiter.enabled = true;
                    it.sel.areas.widget.$root.addClass('wpContentNavWithLimiter');
                }

                it.navigation.updateAnchorOffsets();
                it.navigation.updateLimiterOffsets();
                it.navigation.detectAlign();
                var visible = true;
                var visibleTimeout = false;

                //Hide navigation if dest items (H*) is hidden
                function updateVisibility()
                {
                    if (visibleTimeout !== false) clearTimeout(visibleTimeout);
                    visibleTimeout = setTimeout(function ()
                    {
                        var currentState = sel.$firstAnchor.is(":visible");
                        if (visible === currentState) return;
                        visible = currentState;
                        $root.toggleClass('wpContentNavContent--hidden', !visible);
                    }, 800);
                }

                $('body').click(function ()
                {
                    updateVisibility();
                });
                updateVisibility();


                sel.$controls.each(function ()
                {
                    var $control = $(this);
                    if ($control.next().filter('ul').length !== 0) $control.parent().addClass('wpContentNavHasChild');
                });

                sel.$controls.click(function (e)
                {
                    e.preventDefault();
                    var hash = this.hash;
                    var $a = $(this);
                    var href = $a.attr('href');
                    href = href.replace('#', '');

                    $('html, body').animate({
                        scrollTop: $('a[name="' + href + '"]').offset().top - marginTop
                    }, 1000, function ()
                    {
                        //If use the first method - browser doesn't jump on anchor changing
                        if (history.pushState) history.pushState(null, null, hash);
                        else location.hash = hash;
                    });
                });
            }
        },
        widget: {
            init: function ()
            {
                var sel = it.sel;
                var $root = sel.areas.widget.$root;
                it.common.sticky.init($root);
                it.common.screenType.handlers.push(function (screenType)
                {
                    if (screenType === 'desktop')
                    {
                        sel.navigation.$root.appendTo(sel.areas.widget.$body);
                        $root.removeClass('wpContentNavDisabled');
                        $root.css('display', '');
                        sel.areas.free.$root.addClass('wpContentNavDisabled');
                    } else
                    {
                        sel.navigation.$root.appendTo(sel.areas.free.$body);
                        sel.areas.free.$root.css('min-width', 'initial');
                        $root.addClass('wpContentNavDisabled');
                        sel.areas.free.$root.removeClass('wpContentNavDisabled');
                    }
                    it.navigation.detectAlign();
                });

            }
        },
        free: {
            updateMarginTop: function ()
            {
                var sel = it.sel;
                var position = sel.areas.free.$root.attr('data-wp-content-nav-area-position');
                switch (position)
                {
                    case '':
                    case 'rightMiddle':
                    case 'leftMiddle':
                        sel.areas.free.$root.css('margin-top', sel.areas.free.$root.height() / 2 * -1);
                        break;
                    default:
                        sel.areas.free.$root.css('margin-top', '');
                        break;
                }
            },
            init: function ()
            {
                var sel = it.sel;
                var $toggle = sel.areas.free.$root.find('.wpContentNavHeader .fa');
                var speed = 500;

                $toggle.on('click', function ()
                {
                    var align = 'right';
                    if (sel.areas.free.$root.hasClass('wpContentNavAlignLeft')) align = 'left';
                    var hide = {}, show = {};
                    hide[align] = sel.areas.free.$root.width() * -1;
                    show[align] = 0;

                    if (sel.areas.free.$root.hasClass('wpContentNavAreaContentHide'))
                    {
                        sel.areas.free.$root.animate(
                            hide, speed * 0.5,
                            function ()
                            {
                                sel.areas.free.$root.css('visibility', 'hidden');
                                sel.areas.free.$root.removeClass('wpContentNavAreaContentHide');
                                sel.areas.free.$root.css('min-width', '');
                                it.free.updateMarginTop();
                                var normalWidth = sel.areas.free.$root.outerWidth();
                                sel.areas.free.$root.css(align, normalWidth * -1);
                                sel.areas.free.$root.css('visibility', '');

                                sel.areas.free.$root.animate(
                                    show, speed, function ()
                                    {

                                    }
                                );
                            }
                        );

                    } else
                    {
                        sel.areas.free.$root.animate(
                            hide, speed,
                            function ()
                            {
                                sel.areas.free.$root.addClass('wpContentNavAreaContentHide');
                                sel.areas.free.$root.css('min-width', 'initial');
                                it.free.updateMarginTop();
                                sel.areas.free.$root.animate(show, speed * 0.5);
                            }
                        );
                    }
                });

                it.common.screenType.handlers.push(function (screenType)
                {
                    if (screenType === 'mobile') sel.areas.free.$root.addClass('wpContentNavAreaContentHide');
                    else sel.areas.free.$root.removeClass('wpContentNavAreaContentHide');
                });
            }
        },
        init: function ($root)
        {
            var sel = it.sel;
            sel.areas.widget.$root = $root.closest('.wpContentNavWidget');
            sel.areas.widget.$body = sel.areas.widget.$root.find('#wpContentNavAreaSidebar');
            sel.areas.free.$root = $('#wpContentNavAreaFree');
            sel.areas.free.$body = sel.areas.free.$root.find('.wpContentNavBody');
            sel.navigation.$root = $('#wpContentNavContent');
            if (sel.navigation.$root.length === 0) return;

            var $body = $('body');
            var busy = false;

            function loaded()
            {
                if (sel.areas.widget.$root.length) it.widget.init();
                it.free.init();
                it.common.screenType.init();

                it.navigation.init(sel.navigation.$root);
                it.common.sticky.scroll();
                it.navigation.scroll();
                it.free.updateMarginTop();


                $(window).on('scroll', function ()
                    {
                        if (it.navigation.scrolling) return;
                        it.common.sticky.scroll();
                        it.navigation.scroll();
                    }
                );
                $(window).resize(function ()
                    {
                        if (busy) return;

                        busy = true;
                        it.common.sticky.resize();
                        it.navigation.updateAnchorOffsets();
                        it.navigation.updateLimiterOffsets();
                        it.navigation.scroll();
                        busy = false;
                    }
                );
                it.common.onHeightChange($body, function ()
                    {
                        if (busy) return;

                        busy = true;
                        it.common.sticky.resize();
                        it.common.sticky.scroll();
                        it.navigation.updateAnchorOffsets();
                        it.navigation.updateLimiterOffsets();
                        it.navigation.scroll();
                        busy = false;
                    }
                );

                $body.click(function ()
                {
                    it.navigation.updateAnchorOffsets();
                    it.navigation.updateLimiterOffsets();
                })
            }

            //loading - page is loading
            //interactive - when DOM ready
            //complete - all resources loaded
            if (document.readyState !== "complete")
            {
                $(window).load(function ()
                {
                    loaded();
                });
            } else
            {
                loaded();
            }
        }
    };

    function init()
    {
        var $selector = $('.wpContentNav');
        if ($selector.length > 0) it.init($selector);
    }


    //loading - page is loading
    //interactive - when DOM ready
    //complete - all resources loaded
    if (document.readyState === "loading")
    {
        $(document).ready(function ()
        {
            init();
        });
    } else
    {
        init();
    }

})(jQuery);